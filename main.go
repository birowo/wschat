package main

import (
	"crypto/sha1"
	"encoding/base64"
	"log"
	"net"
	"os"
	"sync"

	"github.com/valyala/fasthttp"
)

var htmCt = "text/html; charset=utf-8"
var rootHtmPool = sync.Pool{
	New: func() any {
		f, err := os.Open("root.htm")
		if err != nil {
			log.Println(err)
			return nil
		}
		return f
	},
}

func unmask(payload []byte, len_ int, maskKey []byte) {
	for i := 0; i < len_; i++ {
		payload[i] ^= maskKey[i&3]
	}
}

const (
	OpCont  = 0x00
	OpTxt   = 0x01
	OpBin   = 0x02
	OpClose = 0x08
	OpPing  = 0x09
	OpPong  = 0x0a
)

func ParseFrame(conn net.Conn, bfr []byte, cb func(fin, opCode byte, reply []byte, hdrLen int)) {
	n := 0
	for {
		for n < 6 {
			n_, err := conn.Read(bfr[n:])
			if err != nil {
				log.Println(err)
				return
			}
			n += n_
		}
		opCode := bfr[0] & 0x0f
		if opCode == OpClose {
			return
		}
		// payload form client to server should be masked
		if (bfr[1] & 0x80) != 0x80 {
			return
		}
		bfr[1] &= 0x7f
		switch bfr[1] {
		case 126:
			len_ := ((int(bfr[2]) << 8) | int(bfr[3]))
			l := 8 + len_
			for n < l {
				n_, err := conn.Read(bfr[n:])
				if err != nil {
					log.Println(err)
					return
				}
				n += n_
			}
			unmask(bfr[8:l], len_, bfr[4:8])
			copy(bfr[4:], bfr[8:l])
			cb(bfr[0]&0x80, opCode, bfr[:4+len_], 4)
			n = copy(bfr, bfr[l:n])
		case 127:
			return //not implemented yet
		case 0:
			cb(bfr[0]&0x80, opCode, bfr[:2], 2)
			n = copy(bfr, bfr[6:n])
		default:
			len_ := int(bfr[1])
			l := 6 + len_
			for n < l {
				n_, err := conn.Read(bfr[n:])
				if err != nil {
					log.Println(err)
					return
				}
				n += n_
			}
			unmask(bfr[6:l], len_, bfr[2:6])
			copy(bfr[2:], bfr[6:l])
			cb(bfr[0]&0x80, opCode, bfr[:2+len_], 2)
			n = copy(bfr, bfr[l:n])
		}
	}
}
func Pong(bfr []byte) {
	bfr[0] ^= 0b11 //pong <- ping
}

const (
	mgcStr   = "258EAFA5-E914-47DA-95CA-C5AB0DC85B11" //magic string
	swkV     = "dGhlIHNhbXBsZSBub25jZQ=="             //Sec-WebSocket-Key
	swaV     = "s3pPLMBiTxaQ9kYGzzhZRbK+xOo="         //Sec-WebSocket-Accept
	swkMSLen = len(swkV) + len(mgcStr)
)

var swaSz = base64.StdEncoding.EncodedLen(sha1.Size)

func swa(swk []byte) []byte {
	var bfr [swkMSLen]byte
	copy(bfr[copy(bfr[:], swk):], mgcStr)
	hash := sha1.Sum(bfr[:])
	base64.StdEncoding.Encode(bfr[:], hash[:])
	return bfr[:swaSz]
}

var conns = make(map[net.Conn]struct{}, 9999)
var mtx sync.RWMutex

func Hijack(conn net.Conn) {
	bfr_ := bfrPool.Get()
	defer func() {
		bfrPool.Put(bfr_)
		mtx.Lock()
		delete(conns, conn)
		mtx.Unlock()
		conn.Close()
	}()
	mtx.Lock()
	conns[conn] = struct{}{}
	mtx.Unlock()
	bfr := bfr_.([]byte)
	ParseFrame(conn, bfr, func(fin, opCode byte, reply []byte, hdrLen int) {
		//payload := reply[hdrLen:]
		if reply == nil {
			return
		}
		switch opCode {
		case OpPing:
			Pong(bfr)
			conn.Write(reply)
		case OpCont, OpTxt, OpBin:
			mtx.RLock()
			for conn_ := range conns {
				mtx.RUnlock()
				conn_.Write(reply)
				mtx.RLock()
			}
			mtx.RUnlock()
		default:
			return
		}
	})
}

var bfrPool = sync.Pool{New: func() any {
	return make([]byte, 1024)
}}
var route = map[string]fasthttp.RequestHandler{
	"/": func(ctx *fasthttp.RequestCtx) {
		f_ := rootHtmPool.Get()
		defer rootHtmPool.Put(f_)
		f, ok := f_.(*os.File)
		if !ok {
			return
		}
		f.Seek(0, 0)
		ctx.SetContentType(htmCt)
		f.WriteTo(ctx)
	},
	"/chat": func(ctx *fasthttp.RequestCtx) {
		ctx.SetStatusCode(fasthttp.StatusSwitchingProtocols)
		ctx.Response.Header.Set("Connection", "Upgrade")
		ctx.Response.Header.Set("Upgrade", "websocket")
		ctx.Response.Header.SetBytesV(
			"Sec-WebSocket-Accept",
			swa(ctx.Request.Header.PeekBytes([]byte("Sec-WebSocket-Key"))),
		)
		ctx.Hijack(Hijack)
	},
}

func main() {
	fasthttp.ListenAndServe(":8080", func(ctx *fasthttp.RequestCtx) {
		if hndlr, ok := route[string(ctx.RequestURI())]; ok {
			hndlr(ctx)
			return
		}
		ctx.Error("", fasthttp.StatusNotFound)
	})
}
